const express = require("express");
const lasoSchema = require("../validations/lasotuvi.validation");
const validator = require("../utils/validator");
const { getHoroscope } = require("../controllers/lasotuvi.controller");

const router = express.Router();

router.post("/", validator(lasoSchema), getHoroscope());

module.exports = router;
