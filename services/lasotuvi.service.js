const { spawn } = require("child_process");
const { json } = require("express");

module.exports = {
  handleHoroscope: async function (data) {
    return new Promise((resolve, reject) => {
      const pythonProcess = spawn("./tools/create_horoscope.exe", [
        data.hoTen,
        data.ngaySinh,
        data.thangSinh,
        data.namSinh,
        data.gioiTinh,
        data.gioSinh,
        data.timeZone,
        data.duongLich,
      ]);

      let pythonOutput = "";

      pythonProcess.stdout.on("data", (data) => {
        pythonOutput = data.toString();
      });

      pythonProcess.stderr.on("data", (data) => {
        console.error(`Python stderr: ${data}`);
      });

      pythonProcess.on("close", (code) => {
        try {
          return resolve(pythonOutput);
        } catch (error) {
          return reject(error);
        }
      });
    });
  },
};
