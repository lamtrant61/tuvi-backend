const asyncHandler = require("../utils/asyncHandler");
const { SuccessResponse } = require("../cores/ApiResponse");
const { handleHoroscope } = require("../services/lasotuvi.service");

const getHoroscope = () => {
  return asyncHandler(async (req, res, next) => {
    const data = await handleHoroscope(req.body);
    return new SuccessResponse("Success", data).send(res);
  });
};

module.exports = {
  getHoroscope,
};
