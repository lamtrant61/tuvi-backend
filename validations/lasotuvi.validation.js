const Joi = require("joi");

const lasoSchema = Joi.object({
  hoTen: Joi.string().min(3).max(50).required(),
  ngaySinh: Joi.number().min(1).max(31).required(),
  thangSinh: Joi.number().min(1).max(12).required(),
  namSinh: Joi.number().min(1).max(9999).required(),
  gioiTinh: Joi.number().allow(0, 1).required(),
  gioSinh: Joi.number().min(0).max(12).required(),
  timeZone: Joi.number().min(0).max(7).required(),
  duongLich: Joi.boolean().required(),
});

module.exports = lasoSchema;
