var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");

const lasotuviRouter = require("./routes/lasotuvi.route");
const { handleError } = require("./middlewares/handleError");
const { NotFoundError } = require("./cores/ApiError");

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/lasotuvi", lasotuviRouter);

app.all("*", (req, res, next) => {
  next(new NotFoundError("Page not found"));
});
app.use(handleError);
module.exports = app;
