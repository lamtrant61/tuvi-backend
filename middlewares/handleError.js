const { ApiError, InternalError, ErrorType } = require("../cores/ApiError");

function handleError(err, req, res) {
  if (err instanceof ApiError) {
    ApiError.handle(err, res);
  } else {
    if (process.env.NODE_ENV === "development") {
      return res.status(500).send(err);
    }
    ApiError.handle(new InternalError(), res);
  }
}

module.exports = {
  handleError,
};
